use 5.010_000;
use strict;
use utf8;
use Test::More;
use Test::Perl::Critic (
  -severity => 4,
  -exclude => [
    'ProhibitConstantPragma',
    'RequireArgUnpacking',
  ]
);

all_critic_ok(
  't/Clawsker.pm',
);
