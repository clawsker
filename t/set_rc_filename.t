use 5.010_000;
use strict;
use utf8;
use Test::More tests => 12;
use Test::Exception;

require_ok ('Clawsker');

use Clawsker;

ok ( defined &Clawsker::set_rc_filename, 'has function' );

is ( $Clawsker::CONFIGRC, 'clawsrc', 'init: rc' );
like ( $Clawsker::CONFIGDIR, qr{^.*\.claws-mail$}, 'init: dir' );

Clawsker::set_rc_filename();
is ( $Clawsker::CONFIGRC, '', 'empty: rc' );
is ( $Clawsker::CONFIGDIR, '', 'empty: dir' );

Clawsker::set_rc_filename('filename');
is ( $Clawsker::CONFIGRC, 'filename', 'fn: rc' );
is ( $Clawsker::CONFIGDIR, '', 'fn: dir' );

Clawsker::set_rc_filename('path/to/filename');
is ( $Clawsker::CONFIGRC, 'filename', 'rp: rc' );
is ( $Clawsker::CONFIGDIR, 'path/to/', 'rp: dir' );

Clawsker::set_rc_filename('/path/to/filename');
is ( $Clawsker::CONFIGRC, 'filename', 'ap: rc' );
is ( $Clawsker::CONFIGDIR, '/path/to/', 'ap: dir' );
